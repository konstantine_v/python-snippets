from bs4 import BeautifulSoup
import requests
import csv

source = requests.get('https://coreyms.com').text
soup = BeautifulSoup(source, 'lxml')

# print out all html formatted
# print(soup.prettify())

# print the title in the html
# match = soup.title.text

csv_file = open('scrape_data.csv', 'w')

csv_writer = csv.writer(csv_file)
csv_writer.writerow(['Headline', 'Summary', 'Date/Time'])

for article in soup.find_all('article'):
    headline = article.h2.a.text
    print(headline)

    summary = article.div.p.text
    print(summary)

    date_time = article.p.time.text
    print(date_time)

    print()
    csv_writer.writerow([headline, summary, date_time])

csv_file.close()

